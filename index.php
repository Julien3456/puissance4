<?php 
session_start();
// require_once ('game.php');
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
   <script src='main.js'></script>
    <title>Puissance 4</title>
</head>
    <body>
    	<aside>
    		<div id="select1">
	    		<label for="player1">Joueur1</label>
	    		<input type="text" name="player1" id="player1" placeholder="Nom du joueur 1">
    		</div>
    		<div id="select2">
	    		<label for="player2">Joueur2</label>
	    		<input type="text" name="player2" id="player2" placeholder="Nom du joueur 2">
    		</div>
    		<div id="commentaire"></div>
		</aside>
		<div class="containCanvas">
			<canvas id="canvas"></canvas>
		</div>
    	<main id="board">
	        <div id="col1" class="col">
	        	<div id="col1row6" class="row6"></div>
	        	<div id="col1row5" class="row5"></div>
	        	<div id="col1row4" class="row4"></div>
	        	<div id="col1row3" class="row3"></div>
	        	<div id="col1row2" class="row2"></div>
	        	<div id="col1row1" class="row1"></div>
	        </div>
	        <div id="col2" class="col">
	        	<div id="col2row6" class="row6"></div>
	        	<div id="col2row5" class="row5"></div>
	        	<div id="col2row4" class="row4"></div>
	        	<div id="col2row3" class="row3"></div>
	        	<div id="col2row2" class="row2"></div>
	        	<div id="col2row1" class="row1"></div>
	        </div>
	        <div id="col3" class="col">
	        	<div id="col3row6" class="row6"></div>
	        	<div id="col3row5" class="row5"></div>
	        	<div id="col3row4" class="row4"></div>
	        	<div id="col3row3" class="row3"></div>
	        	<div id="col3row2" class="row2"></div>
	        	<div id="col3row1" class="row1"></div>
	        </div>
	        <div id="col4" class="col">
	        	<div id="col4row6" class="row6"></div>
	        	<div id="col4row5" class="row5"></div>
	        	<div id="col4row4" class="row4"></div>
	        	<div id="col4row3" class="row3"></div>
	        	<div id="col4row2" class="row2"></div>
	        	<div id="col4row1" class="row1"></div>
	        </div>
	        <div id="col5" class="col">
	        	<div id="col5row6" class="row6"></div>
	        	<div id="col5row5" class="row5"></div>
	        	<div id="col5row4" class="row4"></div>
	        	<div id="col5row3" class="row3"></div>
	        	<div id="col5row2" class="row2"></div>
	        	<div id="col5row1" class="row1"></div>
	        </div>
	        <div id="col6" class="col">
	        	<div id="col6row6" class="row6"></div>
	        	<div id="col6row5" class="row5"></div>
	        	<div id="col6row4" class="row4"></div>
	        	<div id="col6row3" class="row3"></div>
	        	<div id="col6row2" class="row2"></div>
	        	<div id="col6row1" class="row1"></div>
	        </div>
	        <div id="col7" class="col">
	        	<div id="col7row6" class="row6"></div>
	        	<div id="col7row5" class="row5"></div>
	        	<div id="col7row4" class="row4"></div>
	        	<div id="col7row3" class="row3"></div>
	        	<div id="col7row2" class="row2"></div>
	        	<div id="col7row1" class="row1"></div>
	        </div>
	    </main>

    </body>
</html>