<?php 
// session_start();

/**
 * Class de joueur
 */
class Player
{
	private static $ID = 0;
	private $_name;
	private $_iD;
	private $_color;
	private $_turns = 0;
	const COUNTER = 21;

	function __construct($name) {
		self::$ID ++;
		$this -> setId();
		$this -> setName($name);
	}

	private function setId(){
		$this -> _iD = self::$ID;
	}
	public function getId() {
		return $this -> _iD;
	}
	public function setName($name) {
		$this -> _name = $name;
	}
	public function getName(){
		return $this -> _name;
	}

// Compte le nombre de tour
	public function setTurns()	{
		$this -> _turns ++;
	}

// Retourne le nombre de jetons restant
	public function __toString(){
		return self::COUNTER - $this -> _turns;
	}
}

$julien = new Player('Julien');
$noa = new Player('Noa');