document.addEventListener("DOMContentLoaded", () => {
	const player1 = document.getElementById('player1');
	const player2 = document.getElementById('player2');
	const board = document.getElementById('board');
	const col1 = document.getElementById('col1');
	const col2 = document.getElementById('col2');
	const col3 = document.getElementById('col3');
	const col4 = document.getElementById('col4');
	const col5 = document.getElementById('col5');
	const col6 = document.getElementById('col6');
	const col7 = document.getElementById('col7');
	const commentaire = document.getElementById('commentaire');
	const canvas = document.getElementById('canvas')

	let text = "";
	let colorSelect = "";
	let position = "";
	let counter = 0;
	let ctx = canvas.getContext('2d');
	// ctx.font = "30px serif";
	// ctx.textAlign = 'inherit';
	// let centreX = board.clientWidth / 2;
	// let centreY = board.clientHeight / 2;



// Fonction de stockage des infos dans un stockage local
	function storage(perso){
		let rec = sessionStorage.getItem("player1");
		if(rec == null) {
		  sessionStorage.setItem("player1", perso)
		} else {
		  sessionStorage.setItem("player2", perso)
		}
		sessionStorage.setItem("round", "1");
	}

	function endStorage(){
		sessionStorage.clear();
	}

// Fonction pour scruter le plateau, si une fonction renvoie true, c'est gagné
	function isWin(player, playerColor){
		if (searchDiagGrave(playerColor) || searchDiagAcute(playerColor) || searchHorizontalLine(playerColor) || searchVerticalLine(playerColor)) {
			console.log('GAGNER');
			text = "GAGNER"
			drawScore();
			player1.value = "";
			player2.value = "";
			sessionStorage.clear();

			// board.write('Gagné!')
		}
	}

// Fonction pour scruter le plateau en diagonale de droite à gauche, si quatre couleurs joueur alignés, c'est gagné
	function searchDiagGrave(playerColor){
// Controle du diagonale sur la premiere colonne
// On monte d'une ligne de départ
		for (let r = 0; r <= 2; r++) {
			iRow = r;
// On avance d'une colonne de départ
			for (let iCol = 6; iCol >= 0; iCol--) {
				iRow++;
// On controle la position selectionnée et si on sort du cadre on arrete
				position = document.getElementsByClassName('row'+iRow);	
				// if (position[iCol] != null) { 
				// 	console.log(position[iCol]);
				// } else {
				// 	console.log('c\'est null');
				// 	break;
				// }
				if (position[iCol] != null && position[iCol].style.backgroundColor == playerColor) {
					counter++;
				} else {
					counter = 0;
					break; }
				if (counter == 4) {	return true; }
			}
		}
// //Controle du diagonale sur les colonnes suivantes
		for (let r = 0; r <= 2; r++) {
			iRow = 0;
// On avance d'une colonne de départ
			for (let iCol = 5 - r; iCol >= 0; iCol--) {
				iRow++;
// On controle la position selectionnée et si on sort du cadre on arrete
				position = document.getElementsByClassName('row'+iRow);					
				if (position[iCol] != null && position[iCol].style.backgroundColor == playerColor) {
					counter++;
				} else {
					counter = 0;
					break; }
				if (counter == 4) {	return true; }
			}
		}
	}

// Fonction pour scruter le plateau en diagonale de gauche à droite, si quatre couleurs joueur alignés, c'est gagné
	function searchDiagAcute(playerColor){
// Controle du diagonale sur la premiere colonne
// On monte d'une ligne de départ
		for (let r = 1; r <= 3; r++) {
// On avance d'une colonne de départ
			for (let iCol = 0; iCol <= 6; iCol++) {
				iRow = iCol + r;
// On controle la position selectionnée, si la couleue correspond à la couleur du joueur on incrémente le compteur. Si on sort du cadre on arrete
				position = document.getElementsByClassName('row'+iRow);
				// console.log(position[iCol]);
				if (position[iCol] != null && position[iCol].style.backgroundColor == playerColor) {
					counter++;
				} else {
					counter = 0;
					break; }
				if (counter == 4) {	return true; }
			}
		}
//Controle du diagonale sur les colonnes suivantes
		for (let r = 1; r <= 3; r++) {
// On avance d'une colonne de départ
			for (let iCol = r; iCol <= 6; iCol++) {
				iRow = (iCol+1) - r;
// On controle la position selectionnée et si on sort du cadre on arrete
				position = document.getElementsByClassName('row'+iRow);					
				// console.log(position[iCol]);
				if (position[iCol] != null && position[iCol].style.backgroundColor == playerColor) {
					counter++;
				} else {
					counter = 0;
					break; }
				if (counter == 4) { return true; }
			}
		}
	}

// Fonction pour scruter le plateau par ligne, si quatre couleurs joueur alignés, c'est gagné
	function searchHorizontalLine(playerColor){
		for (let iRow = 1; iRow <= 6; iRow++) {
			counter = 0;
			for (let iCol = 0; iCol <= 6; iCol++) {
				position = document.getElementsByClassName('row'+iRow);
				if (position[iCol].style.backgroundColor == playerColor) {
					counter++;
				} else { counter = 0; }
				if (counter == 4) {
					return true;
				}
			}
		}
	}

// Fonction pour scruter le plateau par colonne, si quatre couleurs joueur alignés, c'est gagné
	function searchVerticalLine(playerColor){
		
		for (let iCol = 0; iCol <= 6; iCol++) {
			counter = 0;
			for (let iRow = 1; iRow <= 6; iRow++) {
				position = document.getElementsByClassName('row'+iRow);
				if (position[iCol].style.backgroundColor == playerColor) {
					counter++;
				} else { counter = 0; }
				if (counter == 4) {
					return true;
				}
			}
		}
	}

// Une classe de création des joueurs
	class Player{

		constructor(nom, id, nbChips){
			this.nom = nom;
			this.id = id;
			this.nbChips = nbChips;
		}

		idName(){
			return this.id;
		}
		name(){
			return this.nom;
		}
		nbChips(){
			return this.nbChips;
		}
		color(player){
			if (player.idName() == 1) { return "red";}
			else if (player.idName() == 2) { return "yellow";}
		}
	}

//Enregistrement du joueur 1 quand il s'inscrit
	player1.addEventListener("focusout", (event) => {
		let perso1 = new Player(event.target.value, 1, 4);
		let rec = [perso1.name(), perso1.idName(), perso1.color(perso1)];
		// console.log(rec);
		storage(rec);
	})

//Enregistrement du joueur 2 quand il s'inscrit
	player2.addEventListener("focusout", (event) => {
		let perso2 = new Player(event.target.value, 2, 4);
		let rec = [perso2.name(), perso2.idName(), perso2.color(perso2)];
		storage(rec);
	})

//Quand on click sur le plateau de jeu
		board.addEventListener("click", (event) => {

//Controle de l'enregistrement local
			let player1 = sessionStorage.getItem("player1");
			let player2 = sessionStorage.getItem("player2");

			if (player1 != null && player2 != null) {
				let round = sessionStorage.getItem("round");
				let featurePlayer1 = player1.split(',');
				let featurePlayer2 = player2.split(',');
				let featurePlayer = 'featurePlayer' + round;

				let col = event.target.parentNode.id;
	//Si on ne click pas en dehors de ronds
				if (event.target.tagName != "MAIN" && event.target.className != "col") {
					if (player1 != null && player2 != null){

	//On contrôle chaque lignes de la colonne
						for (let i = 11; i >= 1; i-=2) {
							let row = event.target.parentNode.childNodes[i].className;
							if (event.target.parentNode.childNodes[i].style.backgroundColor == "") {
								if (sessionStorage.getItem("round") == 1) { colorSelect = "red"; }
								if (sessionStorage.getItem("round") == 2) { colorSelect = "yellow"; }

								event.target.parentNode.childNodes[i].style.backgroundColor = colorSelect;

	//Changement de joueur
								if (round == 1) {
									sessionStorage.setItem("round", "2"); 
									// drawScore();
									// commentaire.textContent("Joueur 2 joue");
									isWin(round, featurePlayer1[2]);
								} else {
									sessionStorage.setItem("round", "1");
									// drawScore();
									// commentaire.textContent("Joueur 1 joue");
									isWin(round, featurePlayer2[2]);}
								break;
							}

							if (i == 1 && event.target.parentNode.childNodes[i].style.backgroundColor != "") {
								console.log("La colonne est pleine");
							}
						}
					} else {
						console.log("Il n'y a pas de joueurs enregistrés!")
					}
				}
			} else {
				text = 'Un ou plusieurs joueurs n\'ont pas été enregistrés !';
				// ctx.create_text (10, 10, 'Un ou plusieurs joueurs n\'ont pas été enregistrés !');
				console.log("Un ou plusieurs joueurs n'ont pas été enregistrés !");
				console.log(text);
				drawScore();
			}
		})

		// Fonction d'inscription du score
		function drawScore() {
			ctx.save();
			ctx.font = "30px sans-serif";
			ctx.fillStyle = "gray";
			ctx.textAlign = "center";
			ctx.textBaseline = "middle";
			var centreX = 100 / 2;
			var centreY = 875 / 2;
			ctx.strokeText(text.toString(), 150, 60, 300);
			console.log(text.toString())
			ctx.restore();
		}
	})
